package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import modelo.Arista;
import modelo.Grafo;
import modelo.Vertice;

public class GrafoTest {
	
	@Test
	public void grafoNoNulo() {
		Grafo g = new Grafo();
		
		assertNotNull(g);
	}
	
	@Test
	public void grafoVacio() {
		Grafo g = new Grafo();
		
		assertEquals(0, g.size());
	}
	
	@Test
	public void guardarPrimerVertice() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		
		g.agregarVertice(v1);
		
		assertEquals(v1, g.primerVertice());
	}
	
	@Test
	public void agregarVerticeInvalido() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		
		g.agregarVertice(v1);
		assertFalse(g.verticeValido(v1));
	}
	
	@Test
	public void agregarAristaInvalido() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		Vertice v2 = new Vertice(8, 7);
		
		g.agregarVertice(v1);
		assertFalse(g.aristaValida(new Arista(v2,v1)));
	}
	
	@Test
	public void agregarAristaRepetida() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		Vertice v2 = new Vertice(8, 7);
		Arista a1 = new Arista(v1, v2);
		
		g.agregarVertice(v1);
		g.agregarVertice(v2);
		g.agregarArista(a1);

		assertFalse(g.aristaValida(a1));
	}
	
	@Test
	public void agregarVertices() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		Vertice v2 = new Vertice(8, 7);
		Vertice v3 = new Vertice(7, 4);
		Vertice v4 = new Vertice(6, 1);
		Vertice v5 = new Vertice(3, 1);
		Vertice v6 = new Vertice(1, 9);
		
		List<Vertice> vertices = new ArrayList<>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
				
		g.agregarVertices(vertices);
		assertEquals(vertices.size(), g.size());
	}
	
	@Test
	public void agregarAristas() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		Vertice v2 = new Vertice(8, 7);
		Vertice v3 = new Vertice(7, 4);
		Vertice v4 = new Vertice(6, 1);
		Vertice v5 = new Vertice(3, 1);
		Vertice v6 = new Vertice(1, 9);
		
		List<Vertice> vertices = new ArrayList<>();
		List<Arista> aristas = new ArrayList<>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
		
		aristas.add(new Arista(v1, v2));
		aristas.add(new Arista(v1, v5));
		aristas.add(new Arista(v3, v2));
		aristas.add(new Arista(v3, v4));
		aristas.add(new Arista(v4, v6));
		aristas.add(new Arista(v6, v2));

				
		g.agregarVertices(vertices);
		g.agregarAristas(aristas);
		assertEquals(aristas.size(), g.cantDeAristas());
	}
	
	@Test
	public void grafoCompleto() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		Vertice v2 = new Vertice(8, 7);
		Vertice v3 = new Vertice(7, 4);
		Vertice v4 = new Vertice(6, 1);
		Vertice v5 = new Vertice(3, 1);
		Vertice v6 = new Vertice(1, 9);
		
		List<Vertice> vertices = new ArrayList<>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
		
		g.agregarVertices(vertices);
		g.grafoCompleto();
		
		assertEquals((vertices.size() * (vertices.size()-1))/2, g.cantDeAristas());
	}
}
