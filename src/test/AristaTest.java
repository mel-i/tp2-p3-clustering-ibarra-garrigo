package test;

import static org.junit.Assert.*;

import org.junit.Test;

import modelo.Arista;
import modelo.Vertice;

public class AristaTest {

	@Test
	public void aristasIgualesTest() {
		Vertice v1 = new Vertice(3, 4);
		Vertice v2 = new Vertice(8, 2);
		Arista arista1 = new Arista(v1, v2);
		Arista arista2 = new Arista(v1, v2);
		
		assertTrue(arista1.equals(arista2));
	}
	
	@Test
	public void aristasInversas() {
		Vertice v1 = new Vertice(3, 4);
		Vertice v2 = new Vertice(8, 2);
		Arista arista1 = new Arista(v1, v2);
		Arista arista2 = new Arista(v2, v1);
		
		assertEquals(arista1.inversa(), arista2);
	}
	
	@Test
	public void aristasDistintas() {
		Vertice v1 = new Vertice(3, 4);
		Vertice v2 = new Vertice(8, 2);
		Vertice v3 = new Vertice(5, 7);
		Arista arista1 = new Arista(v1, v2);
		Arista arista2 = new Arista(v2, v3);
		
		assertNotEquals(arista1, arista2);
	}
}
