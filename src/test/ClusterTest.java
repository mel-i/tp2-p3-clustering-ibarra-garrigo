package test;

import org.junit.Before;
import org.junit.Test;

import modelo.Cluster;
import modelo.Vertice;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class ClusterTest {
	
	private Cluster c;
	private List<Vertice> vertices;
	
	@Before
	public void crearInstanciasIniciales() {
		c = new Cluster();
		Vertice v1 = new Vertice(3, 7);
		Vertice v2 = new Vertice(2, 7);
		Vertice v3 = new Vertice(4, 7);
		Vertice v4 = new Vertice(11, 2);
		Vertice v5 = new Vertice(11, 3);
		Vertice v6 = new Vertice(12, 4);
		
		vertices = new ArrayList<>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
		
		List<Vertice> c1 = new ArrayList<>();
		List<Vertice> c2 = new ArrayList<>();
		c1.add(v1);
		c1.add(v2);
		c1.add(v3);
		c2.add(v4);
		c2.add(v5);
		c2.add(v6);
		
		List<List<Vertice>> clust = new ArrayList<>();
		clust.add(c1);
		clust.add(c2);
		
	}
	
	@Test
	public void clusterAutomaticoGrafo() {
		c.llenarGrafo(vertices);
		c.clusteringAutomatico();
		
		assertTrue(c.getClusters().size() >= 2);
	}
	
	@Test
	public void clusterManualGrafo() {
		c.llenarGrafo(vertices);
		c.clusteringManual(3);

		assertEquals(3, c.getClusters().size());
		
	}
	
	@Test
	public void separacionDeClusters() {
		Cluster c = new Cluster();
		List<Vertice> vertices = new ArrayList<>();
		Vertice v1 = new Vertice(3.0, 7.0);
		Vertice v2 = new Vertice(2.0, 7.0);
		Vertice v3 = new Vertice(4.0, 7.0);
		Vertice v4 = new Vertice(11.0, 2.0);
		Vertice v5 = new Vertice(11.0, 3.0);
		Vertice v6 = new Vertice(12.0, 4.0);
		
		vertices = new ArrayList<>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
		
		List<Vertice> c1 = new ArrayList<>();
		List<Vertice> c2 = new ArrayList<>();
		c1.add(v1);
		c1.add(v3);
		c1.add(v2);
		c2.add(v4);
		c2.add(v6);
		c2.add(v5);
		
		List<List<Vertice>> clust = new ArrayList<>();
		clust.add(c1);
		clust.add(c2);
		
		c.llenarGrafo(vertices);
		c.clusteringManual(2);
		
		assertEquals(c1, c.getClusters().get(0));
	}
}
