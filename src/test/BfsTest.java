package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import modelo.Arista;
import modelo.Bfs;
import modelo.Grafo;
import modelo.Vertice;

public class BfsTest {

	@Test(expected = IllegalArgumentException.class)
	public void grafoNulo() {
		Bfs.esConexo(null);
	}

	@Test
	public void grafoCompleto() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		Vertice v2 = new Vertice(8, 7);
		Vertice v3 = new Vertice(7, 4);
		Vertice v4 = new Vertice(6, 1);
		Vertice v5 = new Vertice(3, 1);
		Vertice v6 = new Vertice(1, 9);
		
		List<Vertice> vertices = new ArrayList<>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
		
		g.agregarVertices(vertices);
		g.grafoCompleto();
		
		System.out.println("Grafo Completo");
		
		assertEquals(g.size(), Bfs.bfs(g).size());
		//assertTrue(Bfs.esConexo(g));
	}
	
	@Test
	public void grafoIncompletoConexo() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		Vertice v2 = new Vertice(8, 7);
		Vertice v3 = new Vertice(7, 4);
		Vertice v4 = new Vertice(6, 1);
		Vertice v5 = new Vertice(3, 1);
		Vertice v6 = new Vertice(1, 9);
		
		List<Vertice> vertices = new ArrayList<>();
		List<Arista> aristas = new ArrayList<>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
		
		aristas.add(new Arista(v1, v2));
		aristas.add(new Arista(v1, v5));
		aristas.add(new Arista(v3, v6));
		aristas.add(new Arista(v3, v2));
		aristas.add(new Arista(v6, v1));
		aristas.add(new Arista(v6, v2));
		aristas.add(new Arista(v5, v4));
		aristas.add(new Arista(v5, v6));
		aristas.add(new Arista(v4, v3));
		aristas.add(new Arista(v4, v6));
		aristas.add(new Arista(v2, v4));
		aristas.add(new Arista(v2, v5));
		

				
		g.agregarVertices(vertices);
		g.agregarAristas(aristas);
		assertTrue(Bfs.esConexo(g));
	}
	
	@Test
	public void grafoNoConexo() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		Vertice v2 = new Vertice(8, 7);
		Vertice v3 = new Vertice(7, 4);
		Vertice v4 = new Vertice(6, 1);
		Vertice v5 = new Vertice(3, 1);
		Vertice v6 = new Vertice(1, 9);
		
		List<Vertice> vertices = new ArrayList<>();
		List<Arista> aristas = new ArrayList<>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
		
		aristas.add(new Arista(v1, v4));
		aristas.add(new Arista(v6, v5));
		aristas.add(new Arista(v3, v2));
		aristas.add(new Arista(v4, v6));
		aristas.add(new Arista(v5, v1));
		
		g.agregarVertices(vertices);
		g.agregarAristas(aristas);
		assertFalse(Bfs.esConexo(g));
	}
}
