package test;

import static org.junit.Assert.*;

import org.junit.Test;

import modelo.Arista;
import modelo.Vertice;

public class VerticeTest {

	@Test
	public void verticesIguales() {
		Vertice v1 = new Vertice(1, 2);
		Vertice v2 = new Vertice(1, 2);
		
		assertTrue(v1.equals(v2));
	}
	
	@Test 
	public void verticeIgualVecinosDistintos() {
		Vertice v1 = new Vertice(1, 2);
		Vertice v2 = new Vertice(1, 2);
		Vertice v3 = new Vertice(0, 4);
		
		v1.agregarVecino(v3, new Arista(v1, v3));
		assertNotEquals(v1, v2);
	}
	
	@Test
	public void vecinosSinLoop() {
		Vertice v1 = new Vertice(1, 2);
		Vertice v2 = new Vertice(1, 2);
		
		assertFalse(v1.vecinoPosible(v2));
	}
	
	@Test 
	public void vecinoRepetido() {
		Vertice v1 = new Vertice(1, 2);
		Vertice v2 = new Vertice(0, 4);
		
		v1.agregarVecino(v2, new Arista(v1, v2));
		assertFalse(v1.vecinoPosible(v2));
	}
	
	@Test 
	public void vecinoNoRepetido() {
		Vertice v1 = new Vertice(1, 2);
		Vertice v2 = new Vertice(0, 4);
		Vertice v3 = new Vertice(7, 4);
		
		v1.agregarVecino(v2, new Arista(v1, v2));
		assertTrue(v1.vecinoPosible(v3));
	}
	
	@Test 
	public void eliminarVecino() {
		Vertice v1 = new Vertice(1, 2);
		Vertice v2 = new Vertice(0, 4);
		Arista a = new Arista(v1, v2);
		
		
		v1.agregarVecino(v2, a);
		int tamOriginal = v1.getVecinos().size();
		v1.eliminarVecino(v2, a);
		
		assertNotEquals(tamOriginal, v1.getVecinos().size());
	}
	
	@Test
	public void aristaInvalidaSinVerticeOrigen() {
		Vertice v1 = new Vertice(1, 2);
		Vertice v2 = new Vertice(0, 4);
		Vertice v3 = new Vertice(7, 4);
		
		assertFalse(v1.aristaValida(new Arista(v2, v3)));
	}
	
	@Test
	public void aristaYaExistente() {
		Vertice v1 = new Vertice(1, 2);
		Vertice v2 = new Vertice(7, 4);
		
		v1.agregarVecino(v2, new Arista(v1, v2));
		assertFalse(v1.aristaValida(new Arista(v1, v2)));
	}
	
	@Test
	public void aristaValida() {
		Vertice v1 = new Vertice(1, 2);
		Vertice v2 = new Vertice(0, 4);
		
		assertTrue(v1.aristaValida(new Arista(v1, v2)));
	}
}
