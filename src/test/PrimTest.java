package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import modelo.Arista;
import modelo.Grafo;
import modelo.Prim;
import modelo.Vertice;

public class PrimTest {

	@Test
	public void agmDeGrafoCompleto() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(3, 7);
		Vertice v2 = new Vertice(2, 7);
		Vertice v3 = new Vertice(4, 7);
		Vertice v4 = new Vertice(11, 2);
		Vertice v5 = new Vertice(11, 3);
		Vertice v6 = new Vertice(12, 4);
		
		List<Vertice> vertices = new ArrayList<>();

		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
				
		g.agregarVertices(vertices);
		g.grafoCompleto();
		
		assertEquals(vertices.size() -1, Prim.arbolGeneradorMinimo(g).cantDeAristas());
	}
	
	@Test
	public void agmGrafoConexoIncompleto() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		Vertice v2 = new Vertice(8, 7);
		Vertice v3 = new Vertice(7, 4);
		Vertice v4 = new Vertice(6, 1);
		Vertice v5 = new Vertice(3, 1);
		Vertice v6 = new Vertice(1, 9);
		
		List<Vertice> vertices = new ArrayList<>();
		List<Arista> aristas = new ArrayList<>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
		
		aristas.add(new Arista(v1, v2));
		aristas.add(new Arista(v1, v5));
		aristas.add(new Arista(v3, v6));
		aristas.add(new Arista(v3, v2));
		aristas.add(new Arista(v6, v1));
		aristas.add(new Arista(v6, v2));
		aristas.add(new Arista(v5, v4));
		aristas.add(new Arista(v5, v6));
		aristas.add(new Arista(v4, v3));
		aristas.add(new Arista(v4, v6));
		aristas.add(new Arista(v2, v4));
		aristas.add(new Arista(v2, v5));
				
		g.agregarVertices(vertices);
		g.agregarAristas(aristas);
		
		assertEquals(vertices.size() -1, Prim.arbolGeneradorMinimo(g).cantDeAristas());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void grafoNoConexo() {
		Grafo g = new Grafo();
		Vertice v1 = new Vertice(5, 2);
		Vertice v2 = new Vertice(8, 7);
		Vertice v3 = new Vertice(7, 4);
		Vertice v4 = new Vertice(6, 1);
		Vertice v5 = new Vertice(3, 1);
		Vertice v6 = new Vertice(1, 9);
		
		List<Vertice> vertices = new ArrayList<>();
		List<Arista> aristas = new ArrayList<>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
		
		aristas.add(new Arista(v1, v4));
		aristas.add(new Arista(v6, v5));
		aristas.add(new Arista(v3, v2));
		aristas.add(new Arista(v4, v6));
		aristas.add(new Arista(v5, v1));
		
		g.agregarVertices(vertices);
		g.agregarAristas(aristas);
		
		Prim.arbolGeneradorMinimo(g);

	}
}
