package controller;

import java.util.ArrayList;
import java.util.List;

import org.openstreetmap.gui.jmapviewer.Coordinate;

import modelo.Cluster;
import modelo.Vertice;

public class Controller {
	
	private Cluster cluster;
	private int cantDeClusters;

	public Controller() {
		cluster = new Cluster();
		cantDeClusters = 0;
	}
	
	public void guardarCoordenadas(List<Coordinate> coordenadas) {
		List<Vertice> vertices = convertirCoordenadas(coordenadas);
		cluster.llenarGrafo(vertices);
	}
	
	public void guardarClusters(int cantClusters) {
		cantDeClusters = cantClusters;
	}
	
	public void AutoClustering() {
		cluster.clusteringAutomatico();
	}
	public void ManualClustering() {
		cluster.clusteringManual(cantDeClusters);
	}
	
	public List<List<Coordinate>> getClusters() {
		return convertirVertices(cluster.getClusters());
	}
	
	private List<Vertice> convertirCoordenadas(List<Coordinate> coordenadas){
		List<Vertice> vertices = new ArrayList<>();
		
		for (Coordinate c : coordenadas) {
			vertices.add(new Vertice(c.getLat(), c.getLon()));
		}
		
		return vertices;
	}
	
	private List<List<Coordinate>> convertirVertices(List<List<Vertice>> vertices){
		List<List<Coordinate>> coordenadas = new ArrayList<>();
		
		for (int i = 0; i < vertices.size(); i++) {
			coordenadas.add(listaDeVertices(vertices.get(i)));
		}
		return coordenadas;
	}
	
	private List<Coordinate> listaDeVertices(List<Vertice> vertices){
		List<Coordinate> coordenadas = new ArrayList<>();
		
		for (Vertice v : vertices) {
			coordenadas.add(new Coordinate(v.getX(), v.getY()));
		}
		return coordenadas;
	}
	
	public boolean eleccionGuardada() {
		return cantDeClusters != 0;
	}
	
	public int getClust() {
		return cantDeClusters;
	}
}
