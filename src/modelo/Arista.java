package modelo;

public class Arista implements Comparable<Arista>{

	private Vertice extremo1;
	private Vertice extremo2;
	private double distancia;
	
	public Arista(Vertice v1, Vertice v2) {
		extremo1 = v1;
		extremo2 = v2;
		distancia = calcularDistancia();
	}
	
	private double calcularDistancia() {
		double valor1 = extremo1.getX() - extremo2.getX();
		double valor2 = extremo1.getY() - extremo2.getY();
		
		return Math.sqrt(Math.pow(valor1, 2) + Math.pow(valor2, 2));
	}
	
	public Arista inversa() {
		return new Arista(extremo2, extremo1);
	}
	
	public double getDistancia() {
		return distancia;
	}
	
	public Vertice getExtremo1() {
		return extremo1;
	}
	
	public Vertice getExtremo2() {
		return extremo2;
	}
	
	@Override
	public String toString() {
		return "\nArista[v1" + extremo1.toString() + ", v2" + extremo2.toString() + "] Distancia: " + distancia + "\n";
	}
	
	@Override
	public int compareTo(Arista a) {
		return (int) (this.distancia - a.distancia);
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!(other instanceof Arista)) {
			return false;
		}
		
		Arista o = (Arista)other;
		
		return extremosIguales(o) && this.distancia == o.distancia;
	}
	
	private boolean extremosIguales(Arista other) {
		if (other.extremo1 == null || other.extremo2 == null) {
			return false;
		}
		
		return this.extremo1.equals(other.extremo1) 
				&& this.extremo2.equals(other.extremo2);
	}
}
