package modelo;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Vertice {
	
	private double x;
	private double y;
	private Map<Vertice, Arista> vecinos;
	
	public Vertice(double valor1, double valor2) {
		x = valor1;
		y = valor2;
		vecinos = new HashMap<>();
	}
	
	public void agregarVecino(Vertice v, Arista a) {
		vecinos.put(v, a);
	}
	
	public void eliminarVecino(Vertice v, Arista a) {
		vecinos.remove(v);
	}
	
	public boolean vecinoPosible(Vertice v) {
		return !this.equals(v) && !vecinos.containsKey(v);
	}
	
	public boolean aristaValida(Arista a) {
		return !vecinos.containsValue(a) && this.equals(a.getExtremo1());
	}
	
	public boolean existeVecino(Vertice v) {
		return vecinos.containsKey(v);
	}
	
	public Set<Vertice> getVecinos(){
		return vecinos.keySet();
	}
	
	public Collection<Arista> getAristas(){
		return vecinos.values();
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!(other instanceof Vertice)) {
			return false;
		}
		
		Vertice o = (Vertice)other;
		
		return extremosIguales(o);
	}
	
	private boolean extremosIguales(Vertice otro) {
		return (this.x == otro.x && this.y == otro.y);
	}

	
	@Override
	public String toString() {
		return "Vertice(" + x + ", " + y + ")";
	}
}
