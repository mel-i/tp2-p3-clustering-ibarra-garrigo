package modelo;

import java.util.ArrayList;
import java.util.List;

public class Cluster {

	private Grafo grafoBase;
	private Grafo agm;
	private List<Arista> aristas;
	private List<List<Vertice>> clusters;
	private int cantDeClusters;
	
	public Cluster() {
		grafoBase = new Grafo();
		clusters = new ArrayList<>();
		cantDeClusters = 0;
	}
	
	private void iniciarGrafo() {
		grafoBase.grafoCompleto();
		agm = Prim.arbolGeneradorMinimo(grafoBase);
		
	}
	
	public void clusteringAutomatico() {
		iniciarGrafo();
		eliminacionPorPromedio();
		obtenerClusters();
	}
	
	public void clusteringManual(int cantDeClusters) {
		iniciarGrafo();		
		eliminacionPorCantidad(cantDeClusters);
		obtenerClusters();
	}
	
	/*---------- eliminacion de aristas ---------*/
	
	private void eliminacionPorPromedio() {
		double distanciaPromedio = agm.promedioDeAristas();
		arreglarAristas();
		
//		System.out.println("distanciaPromedio " + distanciaPromedio);
//		System.out.println("aristas pre eliminacion: " + agm.getAristas());
		int i = 0;
		Arista a = aristas.get(0);
		while(a.getDistancia() >= distanciaPromedio && i < 9) {
			agm.eliminarArista(a);			
			a = aristas.get(0);
			i++;
		}
//		System.out.println("aristas post eliminacion: " + agm.getAristas());
	}
	
	private void eliminacionPorCantidad(int cantidadDeClusters) {
		arreglarAristas();

//		System.out.println("aristas pre eliminacion: " + agm.getAristas());
		for (int i = 0; i < cantidadDeClusters -1; i++) {
			agm.eliminarArista(aristas.get(0));
		}
//		System.out.println("aristas post eliminacion: " + agm.getAristas());

	}
	
	/*---------- agrupacion de nuevos clusters  ---------*/
	
	private void obtenerClusters() {
		clusters = new ArrayList<>();
	
		while(!agm.isEmpty()) {
			clusters.add(Bfs.bfs(agm));
			
			agm.eliminarVertices(clusters.get(cantDeClusters));
			cantDeClusters++;
		}	
	}
	
	private List<Arista> arreglarAristas() {
		aristas = agm.getAristas();
		Arista aux = null;
		Arista aux2 = null;
		for(int i = 0; i < aristas.size(); i++){  
			for(int j = 1; j < (aristas.size() - i); j++){  
				if(aristas.get(j -1).getDistancia() < aristas.get(j).getDistancia()){    
					aux = aristas.get(j -1);  
					aux2 = aristas.get(j); 
					aristas.set(j, aux);
					aristas.set(j - 1, aux2);  
				}  
			} 
		}
		
		return aristas;
	}

	public void llenarGrafo(List<Vertice> vertices) {
		grafoBase.agregarVertices(vertices);
	}
	
	public List<List<Vertice>> getClusters(){
		return clusters;
	}
}
