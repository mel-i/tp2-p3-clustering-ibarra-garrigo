package modelo;

import java.util.ArrayList;
import java.util.List;

public class Bfs {

	private static boolean [] verticesMarcados;
	private static List<Vertice> vertices;
	private static List<Vertice> todosLosVertices;
	private static List<Arista> aristas;

	public static boolean esConexo(Grafo g) {
		if (g == null) {
			throw new IllegalArgumentException("El grafico es null");
		}
		if (g.size() == 0){
			return true;
		}
		return bfs(g).size() == g.size();
	}

	private static void iniciarBfs(Grafo g) {
		vertices = new ArrayList<>();
		todosLosVertices = g.getVertices();
		vertices.add(todosLosVertices.get(0));
		verticesMarcados = new boolean[todosLosVertices.size()];
		aristas = g.getAristas();
	}

	private static void agregarVecinosPendientes(Vertice v){		
		for (Vertice vecino : v.getVecinos()){
			int indice = todosLosVertices.indexOf(vecino);
			
			if (indice >= 0 && indice < verticesMarcados.length) {
				if (!vertices.contains(vecino) && existeConexion(v, vecino) 
					&& !verticesMarcados[todosLosVertices.indexOf(vecino)]){
					vertices.add(vecino);
				}
			}
		}
	}
	
	private static boolean existeConexion(Vertice v1, Vertice v2) {
		boolean conexion = false;
		
		for (Arista a : aristas) {
			if ((a.getExtremo1().equals(v1) && a.getExtremo2().equals(v2)) || 
					(a.getExtremo1().equals(v2) && a.getExtremo2().equals(v1))) {
				conexion = true;
				break;
			}
		}
		
		return conexion;
	}
	
	public static List<Vertice> bfs(Grafo g) {
		iniciarBfs(g);
		List<Vertice> retorno = new ArrayList<>();

		while(vertices.size() > 0) {
			int i = todosLosVertices.indexOf(vertices.get(0));
			verticesMarcados[i] = true;
			retorno.add(vertices.get(0));
			agregarVecinosPendientes(vertices.get(0));
			vertices.remove(vertices.get(0));	
			
		}
		return retorno;
	}
}
