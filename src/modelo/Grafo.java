package modelo;

import java.util.ArrayList;
import java.util.List;


public class Grafo {
	
	private List<Vertice> vertices;
	private List<Arista> aristas;
	
	public Grafo() {
		vertices = new ArrayList<>();
		aristas = new ArrayList<>();
	}
	
	/*------- Vertices -------*/
	
	public void agregarVertice(Vertice v) {
		if (!verticeValido(v)) {
			throw new IllegalArgumentException("El vertice es null");
		}
		if (!contieneVertice(v)) {
			vertices.add(v);
		}
	
	}
	
	public void agregarVertices(List<Vertice> verticesUsuario) {
		for (Vertice v : verticesUsuario) {
			if (!verticeValido(v)) {
				throw new IllegalArgumentException("Uno de los vetices es null");
			}
			if (!contieneVertice(v)) {
				vertices.add(v);
			}
		}
	}

	public boolean contieneVertice(Vertice v) {
		return vertices.contains(v);
	}

	public boolean verticeValido(Vertice v) {
		return !(v == null);
	}

	public void eliminarVertices(List<Vertice> eliminados) {
		for (Vertice v : eliminados) {
			if (vertices.contains(v)) {
				vertices.remove(v);
			}
		}
	}
	
	/*------- Aristas -------*/
	
	public void agregarAristas(List<Arista> aristasUsuario) {
		for (Arista a : aristasUsuario) {
			if (!aristaValida(a)) {
				throw new IllegalArgumentException("Una de las aristas no es valida");
			}
			crearVinculo(a);
			aristas.add(a);
		}
	}
	
	public void agregarArista(Arista a) {
		if (!aristaValida(a)) {
			throw new IllegalArgumentException("No es posible agregar la arista");
		}
		crearVinculo(a);
		aristas.add(a);
	}
	
	public boolean contieneArista(Arista a) {
		return aristas.contains(a);
	}
	
	public boolean aristaValida(Arista a) {
		if (a == null) {
			return false;
		}
		
		return (!aristas.contains(a) && !aristas.contains(a.inversa())) &&
				(vertices.contains(a.getExtremo1()) && vertices.contains(a.getExtremo2()));
	}
	
	public void eliminarAristas(List<Arista> eliminados) {
		for (Arista a : eliminados) {
			if (aristas.contains(a)) {
				eliminarArista(a);
			}
		}
	}
	
	public void eliminarArista(Arista a) {
		eliminarVinculo(a);
		aristas.remove(a);
	}
	
	/*-----------------------*/
	
	private void crearVinculo(Arista a) {
		Vertice v1 = a.getExtremo1();
		Vertice v2 = a.getExtremo2();
		v1.agregarVecino(v2, a);
		v2.agregarVecino(v1, a.inversa());
	}
	
	private void eliminarVinculo(Arista a) {
		Vertice v1 = a.getExtremo1();
		Vertice v2 = a.getExtremo2();
		v1.eliminarVecino(v2, a);
		v2.eliminarVecino(v1, a.inversa());
	}
	
	
	public void grafoCompleto() {
		for (Vertice v : vertices) {
			agregarAlVecindario(v);
		}
	}

	private void agregarAlVecindario(Vertice vecinoNuevo) {
		for (Vertice v : vertices) {
			Arista a = new Arista(vecinoNuevo, v);
			
			if (aristaValida(a) && !v.equals(vecinoNuevo)) {
				aristas.add(a);
				crearVinculo(a);
			}
		}
	}
	
	public double promedioDeAristas() {
		double sumatoria = 0;
		for (Arista a : aristas) {
			sumatoria += a.getDistancia();
		}
		return (sumatoria / aristas.size());
	}
	
	public Vertice primerVertice() {
		return vertices.get(0);
	}
	
	public boolean isEmpty() {
		return vertices.size() == 0;
	}
	
	public List<Vertice> getVertices() {
		return vertices;
	}
	
	public List<Arista> getAristas() {
		return aristas;
	}
	
	public int cantDeAristas() {
		return aristas.size();
	}
	
	public int size() {
		return vertices.size();
	}
}
