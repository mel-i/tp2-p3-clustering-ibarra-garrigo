package modelo;

import java.util.ArrayList;
import java.util.List;

public class Prim {
	
	private static Grafo agm;
	private static List<Arista> aristasPendientes;
	private static List<Arista> aristasProhibidas;
	private static Vertice verticeActual;
	private static Arista aristaMasBarata;
	
	public static Grafo arbolGeneradorMinimo(Grafo g) {
	if (g == null) {
		throw new IllegalArgumentException("No se puede hacer un agm de un grafo nulo");
	}
	if (!Bfs.esConexo(g)) {
		throw new IllegalArgumentException("No se puede hacer un agm de un grafo no conexo");
	}
	if (g.size() <= 1) {
		return g;
	}
	
	agm = new Grafo();
	aristasPendientes = new ArrayList<>();
	aristasProhibidas = new ArrayList<>();
	verticeActual = g.primerVertice();
	arbolGeneradorMin(g.size());
	return agm;
	}
	
	private static void arbolGeneradorMin(int cantDeVertices) {
		agm.agregarVertice(verticeActual);
		while (agm.size() != cantDeVertices) {
			obtenerAristas();
			aristaMasBarata = obtenerAristaMasBarataSinCiclo();
			verticeActual = actualizarVertice();
			actualizarGrafo();
		}
	}
	
	private static void obtenerAristas() {

		for (Arista a : verticeActual.getAristas()) {
				aristasPendientes.add(a);
		}
	}
	
	private static Arista obtenerAristaMasBarataSinCiclo() {
		arreglarAristas();

		Arista candidata = aristasPendientes.get(0);
		double minima = candidata.getDistancia();
		for (int i = 1; i < aristasPendientes.size(); i++) {
			if (!aristasProhibidas.contains(aristasPendientes.get(i)) && !aristasProhibidas.contains(aristasPendientes.get(i).inversa())) {
				if (aristasPendientes.get(i).getDistancia() <= minima && !agm.contieneVertice(aristasPendientes.get(i).getExtremo2())){
					candidata = aristasPendientes.get(i);
					minima = candidata.getDistancia();
				}
			}
		}
		aristasProhibidas.add(candidata);
		aristasPendientes.remove(candidata);
		return candidata;
	}
	
	private static void arreglarAristas() {
		Arista aux = null;
		Arista aux2 = null;
		for(int i = 0; i < aristasPendientes.size(); i++){  
			for(int j = 1; j < (aristasPendientes.size() - i); j++){  
				if(aristasPendientes.get(j -1).getDistancia() < aristasPendientes.get(j).getDistancia()){    
					aux = aristasPendientes.get(j -1);  
					aux2 = aristasPendientes.get(j); 
					aristasPendientes.set(j, aux);
					aristasPendientes.set(j - 1, aux2);  
				}  
			} 
		}
	}
	
	private static Vertice actualizarVertice() {
		return aristaMasBarata.getExtremo2();
	}
	
	private static void actualizarGrafo() {	
		agm.agregarVertice(verticeActual);
		agm.agregarArista(aristaMasBarata);

	}
}
