package visual;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerCircle;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import java.util.List;

@SuppressWarnings("serial")
public class VentanaEstadistica extends Ventana {

	private JPanel panelMapa;
	private JMapViewer mapa;
	private Coordinate coordenadaInicial;
	private JTable table;
	private DefaultTableModel datosTabla;
	private List<List<Coordinate>> clusters;
	private Color[] colores;

	public VentanaEstadistica() {
		super();
		crearFrame();
	}

	@Override
	protected void crearFrame() {
		setTitle("Cluster");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 100, 600, 500);
		
		colores = new Color[]{Color.RED, Color.BLUE, Color.GRAY, Color.CYAN, Color.MAGENTA,
				Color.GREEN, Color.ORANGE, Color.PINK, Color.YELLOW,  Color.LIGHT_GRAY};
		eleccionUsuario = false;

		JLabel lblTitulo = new JLabel("Estadisticas");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblTitulo.setBounds(201, 0, 161, 42);
		getContentPane().add(lblTitulo);
		
		datosTabla = new DefaultTableModel(
				new Object[][] {},
				new String[] {"Clust N�", "X", "Y"}); 
		table = new JTable(datosTabla);
		
		JScrollPane scroll =new JScrollPane(table);
		scroll.setLocation(389, 52);
		scroll.setSize(187, 387);
		getContentPane().add(scroll);
		
		panelMapa = new JPanel();
		panelMapa.setBounds(20, 52, 354, 387);
		getContentPane().add(panelMapa);
		
		mapa = new JMapViewer();
		panelMapa.add(mapa);
		mapa.setZoomControlsVisible(false);
		coordenadaInicial = new Coordinate(-34.52202,-58.70002);
		mapa.setLayout(null);
		mapa.setDisplayPosition(coordenadaInicial, 8);
	}
	
	private void rellenarTabla() {
		clusters = controlador.getClusters();
		Object[] fila = new Object[3];

		for (List<Coordinate> cluster : clusters) {
			fila[0] = clusters.indexOf(cluster) + 1;
			for (Coordinate c : cluster) {
				fila[1] = c.getLat();
				fila[2] = c.getLon();
				datosTabla.addRow(fila);
			}
		}
	}
	
	private void dibujarClusters() {
		Color colorCluster = colores[0];
		
		for (List<Coordinate> clust : clusters) {
			colorCluster = colores[clusters.indexOf(clust)];
			MapPolygon poligono = new MapPolygonImpl(clust);
			mapa.addMapPolygon(poligono);
			actualizarPantalla();
			for (Coordinate c : clust) {
				MapMarkerCircle m = new MapMarkerDot(c);
				m.setBackColor(colorCluster);;
				mapa.addMapMarker(m);
			}
		}
	}
	
	@Override
	public void eleccionUsuario(boolean eleccionDelUsuario) {
		if (eleccionDelUsuario) {
			controlador.ManualClustering();
		} else {
			controlador.AutoClustering();
		}
		rellenarTabla();
		dibujarClusters();
	}
}
