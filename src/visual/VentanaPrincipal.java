package visual;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class VentanaPrincipal extends Ventana{
	
	private JButton btnAutomatico;
	private JButton btnManual;

	public VentanaPrincipal() {
		super();
		crearFrame();
	}

	@Override
	protected void crearFrame() {
		setTitle("Cluster");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 100, 600, 500);
		getContentPane().setLayout(null);
		
		JLabel lblClustering = new JLabel("Clustering");
		lblClustering.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblClustering.setForeground(new Color(0, 0, 128));
		lblClustering.setHorizontalAlignment(SwingConstants.CENTER);
		lblClustering.setBounds(193, 92, 177, 41);
		getContentPane().add(lblClustering);
		
		btnAutomatico = new JButton("Cluster Automatico");
		btnAutomatico.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnAutomatico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				avanzar(false);
			}
		});
		btnAutomatico.setBounds(139, 211, 294, 66);
		getContentPane().add(btnAutomatico);
		
		btnManual = new JButton("Elegir cantidad de Clusters");
		btnManual.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnManual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				avanzar(true);
			}
		});
		btnManual.setBounds(139, 308, 294, 60);
		getContentPane().add(btnManual);
	}
	
	private void avanzar(boolean eleccionDeClusters) {
		siguienteVentana = new VentanaMapa();
		siguienteVentana.eleccionUsuario(eleccionDeClusters);
		cambiarVentana();
	}

}
