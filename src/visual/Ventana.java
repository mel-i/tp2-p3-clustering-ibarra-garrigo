package visual;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Controller;

@SuppressWarnings("serial")
public abstract class Ventana extends JFrame {

	protected JPanel contentPane;
	protected Ventana siguienteVentana;
	protected Controller controlador;
	protected boolean eleccionUsuario;

	public Ventana() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		eleccionUsuario = false;
	}
	
	protected abstract void crearFrame();
	
	protected void actualizarPantalla() {
		validate();
		repaint();
	}

	protected void cambiarVentana() {
		siguienteVentana.setVisible(true);
		this.setVisible(false);;
	}
	
	protected void setSiguienteVentana(Ventana ventana) {
		siguienteVentana = ventana;
	}
	
	public void agregarVentanaSig(Ventana v) {
		if (siguienteVentana == null) {
			setSiguienteVentana(v);
		}
	}
	
	public void agregarControler(Controller c) {
		if (controlador == null) {
			controlador = c;
		}
	}
	
	public void eleccionUsuario(boolean b) {
		eleccionUsuario = b;
	}
}
