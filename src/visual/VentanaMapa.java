package visual;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;

import controller.Controller;

import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;

@SuppressWarnings("serial")
public class VentanaMapa extends Ventana{
	
	private JPanel panelMapa;
	private JMapViewer mapa;
	private List<Coordinate> coordenadas;
	private Coordinate coordenadaInicial;
	private JSpinner cantClusters;
	private JLabel lblInfo;

	public VentanaMapa() {
		super();
		coordenadas = new ArrayList<>();
		controlador = new Controller();
		siguienteVentana = new VentanaEstadistica();
		siguienteVentana.agregarControler(controlador);
		crearFrame();
	}

	@Override
	protected void crearFrame() {
		setTitle("Cluster");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 100, 600, 500);
		
		panelMapa = new JPanel();
		panelMapa.setBounds(25, 44, 551, 321);
		getContentPane().add(panelMapa);
		
		mapa = new JMapViewer();
		panelMapa.add(mapa);
		mapa.setZoomControlsVisible(false);
		coordenadaInicial = new Coordinate(-34.52202,-58.70002);
		
		mapa.setLayout(null);
		mapa.setDisplayPosition(coordenadaInicial, 8);
		
		JLabel lblinstruccion = new JLabel("Indique las coordenadas en el mapa");
		lblinstruccion.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblinstruccion.setHorizontalAlignment(SwingConstants.CENTER);
		lblinstruccion.setBounds(32, 10, 218, 24);
		getContentPane().add(lblinstruccion);
		
		lblInfo = new JLabel("Cant de clusters: ");
		lblInfo.setHorizontalAlignment(SwingConstants.CENTER);
		lblInfo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblInfo.setBounds(25, 396, 117, 17);
		getContentPane().add(lblInfo);
		
		SpinnerModel modelo = new SpinnerNumberModel(2, 2, 10, 1);
		cantClusters = new JSpinner(modelo);
		cantClusters.setBounds(176, 396, 30, 20);
		getContentPane().add(cantClusters);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardarDatos();
			}

		});
		btnGuardar.setBounds(449, 395, 85, 21);
		getContentPane().add(btnGuardar);
		
		mapa.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                guardarCoordenada(e.getPoint());
            }
        });
		
	}
	
	private void guardarCoordenada(Point punto) {
		crearMarcador((Coordinate) mapa.getPosition(punto));
	}

	private void crearMarcador(Coordinate coord) {
		coordenadas.add(coord);
		mapa.addMapMarker(new MapMarkerDot(coord));
	}
	
	private void guardarDatos() {
		if (coordenadas.isEmpty()) {
			JOptionPane.showMessageDialog(getContentPane(), "        La lista esta vacia, \nIngrese alguna coordenada");
		} else {
			controlador.guardarCoordenadas(coordenadas);
			if (eleccionUsuario) { // cant de clusters a desicion del usuario
				controlador.guardarClusters((int) cantClusters.getValue());
			}
			controlador.guardarCoordenadas(coordenadas);
			siguienteVentana.eleccionUsuario(eleccionUsuario);
			cambiarVentana();
		}
	}
	
	@Override
	public void eleccionUsuario(boolean b) {
		lblInfo.setVisible(b);
		cantClusters.setVisible(b);
		eleccionUsuario = b;
	}
}
